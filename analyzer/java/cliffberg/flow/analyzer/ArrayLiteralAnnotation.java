package cliffberg.flow.analyzer;

import cliffberg.symboltable.Annotation;
import cliffberg.flow.parser.FlowParser.ExprContext;

import org.antlr.v4.runtime.ParserRuleContext;

import java.util.List;

/**
 A re-writing of an array literal. This is needed because array literals are sometimes
 implicitly expanded. We want to avoid rewriting the actual parse tree, so we do the
 re-writing as an annotation.
 */
public class ArrayLiteralAnnotation implements Annotation {

	public ParserRuleContext possibleArrayLiteral; // an array_literal or expr node
	public List<ExprContext> exprs;  // the elements of the literal

	public ArrayLiteralAnnotation(ParserRuleContext possibleArrayLiteral, List<ExprContext>	 exprs) {
		this.possibleArrayLiteral = possibleArrayLiteral;
		this.exprs = exprs;
	}
}
