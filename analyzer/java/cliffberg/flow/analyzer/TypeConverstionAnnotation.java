package cliffberg.flow.analyzer;

import org.antlr.v4.runtime.ParserRuleContext;
import cliffberg.symboltable.Annotation;

public class TypeConverstionAnnotation implements Annotation {

	public ParserRuleContext node;
	public FlowDataType fromType;
	public FlowDataType toType;

	public TypeConverstionAnnotation(ParserRuleContext node,
		FlowDataType fromType,
		FlowDataType toType) {

		this.node = node;
		this.fromType = fromType;
		this.toType = toType;
	}
}
