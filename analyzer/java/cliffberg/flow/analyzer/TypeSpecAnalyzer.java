package cliffberg.flow.analyzer;

import static cliffberg.flow.analyzer.FlowDataType.*;
import static cliffberg.utilities.AssertionUtilities.*;

import cliffberg.flow.parser.FlowParser.*;

import cliffberg.symboltable.NameResolution;
import cliffberg.symboltable.ExprAnnotation;
import cliffberg.symboltable.CompilerState;
import cliffberg.symboltable.TypeDescriptor;
import cliffberg.symboltable.SymbolWrapper;
import cliffberg.symboltable.DeclaredEntry;
import cliffberg.symboltable.SymbolEntry;
import cliffberg.symboltable.SymbolTable;
import cliffberg.symboltable.NameScope;
import cliffberg.symboltable.NameScopeEntry;
import cliffberg.symboltable.ValueType;
import cliffberg.symboltable.VisibilityChecker;
import cliffberg.symboltable.UnknownTypeException;
import cliffberg.symboltable.ConversionException;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.Set;
import java.util.TreeSet;
import java.util.List;
import java.util.LinkedList;

/**
 For interpreting the type of a type spec. If the type spec applies to a symbol, then
 the symbol's symbol table entry is annotated with a FlowTypeDescriptor. If a type cannot
 be determined, an exception is thrown.

 Preconditions:

 	* Values and expressions have been type-analyzed (see the StaticEvaluator class).
 	* All symbol definitions have been attributed with symbol table entries, except for value definitions
		that are ambiguous. (See Analyzer Design, the section "Actions, By Pass", and the pass
		labeled "Identify value_defs that are declarations".)

 Side effects:

 	* For those methods that have a DeclaredEntry argument, the symbol entry's typeDescriptor is set.
 	* The type spec node (Value_type_specContext) is attributed with the type descriptor.

 Package-visible methods:

 	analyzeTypeSpec(DeclaredEntry, Value_type_specContext) -
 		Use this to analyze the type of a declared symbol that has a type spec.

 	analyzeEntryType(DeclaredEntry) -
 		Analyze the type of a symbol table entry. If the entry's type has been analyzed,
 		use that; otherwise, find its type spec and analyze it. The entry is assumed to be
 		a value_def or a type_def. Calls the above method if the symbol has a value_type_spec.
 		Entry can be any type, including an object, class, contract, or conduit.

 	analyzeTypeSpec(Value_type_specContext) -
 		Analyze the specified type spec, which is presumed to not have a symbol entry.

 	analyzeCondRetTpSpec(Conduit_ret_type_specContext) -
 		Analyze a method return type that specifies a conduit. The return type will be
 		a StaticConduitTypeDescriptor.

 	analyzeIntrinsicType(Intrinsic_type_specContext intrinsicTypeSpec) -
 		Interpret an intrinsic type spec and return the corresponding FlowDataType.

 	getValueTypeSpec(ParseTree) -
 		Given a node that defines a value or value type, find the nested value_type_spec
 		node that specifies the type.
 */
public class TypeSpecAnalyzer {

	private NameResolution<FlowValueType, ParseTree, ParseTree, ParseTree, TerminalNode> nameResolver;
	private CompilerState<FlowValueType, ParseTree, ParseTree, TerminalNode> state;
	private SymbolWrapper<ParseTree> symbolWrapper;
	private VisibilityChecker visibilityChecker;
	private Set<ParseTree> analyzedSet = new TreeSet<ParseTree>();

	TypeSpecAnalyzer(NameResolution<FlowValueType, ParseTree, ParseTree, ParseTree, TerminalNode> nameResolver,
		CompilerState<FlowValueType, ParseTree, ParseTree, TerminalNode> state, SymbolWrapper<ParseTree> symbolWrapper,
		VisibilityChecker visibilityChecker) {

		this.nameResolver = nameResolver;
		this.state = state;
		this.symbolWrapper = symbolWrapper;
		this.visibilityChecker = visibilityChecker;
	}

	/**
	 Determine the 'type' of the value_type_spec, and (if entry is not null) set the entry's
	 type descriptor. It is assumed that all symbol names have been resolved when this method
	 is called.
	 */

	FlowTypeDescriptor analyzeTypeSpec(DeclaredEntry<FlowValueType, ParseTree, TerminalNode> entry,
		Value_type_specContext valueTypeSpec) throws Exception {

		if ((entry != null) && (entry.getTypeDesc() != null)) return (FlowTypeDescriptor)(entry.getTypeDesc());

		// Detect circular analysis:
		if (checkAnalyzed(valueTypeSpec)) throwLinePosExc(valueTypeSpec, "Circular reference");

		/* value_type_spec	: intrinsic_type_spec
							| qual_name
							| struct_type_spec
							| enum_spec_scope
							| array_elt_type_spec array_def_spec+
							;
			*/

		if (valueTypeSpec.intrinsic_type_spec() != null) {
			/* an intrinsic value */
			return analyzeType(entry, valueTypeSpec.intrinsic_type_spec());

		} else if (valueTypeSpec.qual_name() != null) {
			/* borrows an existing type */
			return analyzeType(entry, valueTypeSpec.qual_name(), valueTypeSpec);

		} else if (valueTypeSpec.struct_type_spec() != null) {
			/* defines a new struct type */
			return analyzeType(entry, valueTypeSpec.struct_type_spec(), valueTypeSpec);

		} else if (valueTypeSpec.enum_spec_scope() != null) {
			/* Defines a new enum or enum type */
			return analyzeType(entry, valueTypeSpec.enum_spec_scope(), valueTypeSpec);

		} else if (valueTypeSpec.array_def_spec() != null) {
			/* An array */
			return analyzeType(entry, valueTypeSpec.array_def_spec(), valueTypeSpec);

		} else
			throw new RuntimeException("Unexpected value_type_spec condition");
	}

	/**
	 Analyze the declared type of a symbol table entry. If the entry's type has been analyzed,
	 use that; otherwise, find its type spec and analyze it.
	 */
	FlowTypeDescriptor analyzeEntryType(DeclaredEntry<FlowValueType, ParseTree, TerminalNode> typeEntry) throws Exception {

		ParseTree def = typeEntry.getDefiningNode();

		/* Defining node can be:
			value_def, type_def, class_def, contract_def, conduit_spec, object_create_spec

			Other DeclaredEntry defining nodes for which a type desc is not created:

			namespace, function_def, method_signature, constructor_def, arg_def, catch_clause, for_stmt
		 */

		if ((def instanceof Value_defContext) || (def instanceof Type_defContext)) {
			/*
			value_def				: ( Tvalue | Tconstant )? value_decl ;
			type_def				: Ttype type_decl ;
			*/
			Value_type_specContext valueTypeSpec = getValueTypeSpec(def);
			return analyzeTypeSpec(typeEntry, valueTypeSpec);

		} else if ((def instanceof Class_defContext) || (def instanceof Contract_defContext)) {
			/*
			class_def				: concurrent_spec? Tclass Tident is_seq?
										Tl_curl_br class_scope Tr_curl_br ;
			contract_def			: Tcontract Tident is_seq?
										Tl_curl_br contract_def_scope Tr_curl_br ;
			*/
			NameScope<FlowValueType, ParseTree, TerminalNode> nameScope = this.state.getScope(def);

			boolean isClass = (def instanceof Class_defContext);
			boolean isContract = (def instanceof Contract_defContext);

			Is_seqContext isSeq = null;
			if (isClass) isSeq = ((Class_defContext)def).is_seq();
			if (isContract) isSeq = ((Contract_defContext)def).is_seq();

			if (isSeq == null) return new ClassOrContractTypeDescriptor(nameScope);

			// There are appended classes or contracts: we must identify those class/contract types:
			Set<ClassOrContractTypeDescriptor> composedOfTypes = new TreeSet<ClassOrContractTypeDescriptor>();
			for (Qual_nameContext qualName : isSeq.qual_name()) { // each qual_name in is_seq
				/* Get the actual (not borrowed/alias) type of specified by the qual_name: */

				// Get the definition for qual_name:
				List<TerminalNode> qualNameIds = qualName.Tident();
				DeclaredEntry<FlowValueType, ParseTree, TerminalNode> qualNameEntry = this.state.getDef(qualNameIds.get(qualNameIds.size()-1));
				if (qualNameEntry == null) { // attempt to resolve it now
					SymbolEntry e = this.nameResolver.resolveAndBindSymbol(
						qualNameIds, this.visibilityChecker);
					if (e == null) {
						throwLinePosExc(qualName, "Unable to identify");
					}
					qualNameEntry = (DeclaredEntry<FlowValueType, ParseTree, TerminalNode>)e;
				}

				ParseTree qualNameDef = qualNameEntry.getDefiningNode();

				if (! (qualNameDef instanceof Class_defContext) || (qualNameDef instanceof Contract_defContext)) {
					// qualName's def is not a class or contract
					throwLinePosExc(qualName, "Expected a class or contract name");
				}

				if (isContract) { // may only extend contracts
					if (! (qualNameDef instanceof Contract_defContext)) {
						throwLinePosExc(qualName, "Expected a contract name");
					}
				}

				if (isClass) {
					if (qualNameDef instanceof Contract_defContext) {
						/* ....Semantic check: that the class implements the contract: */

					}
				}

				/* Determine type of the declared entry represented by qualName: */

				TypeDescriptor<FlowValueType> isTypeDesc = this.state.getTypeDesc(qualNameDef);
				if (isTypeDesc == null) { // try to determine type now:
					isTypeDesc = analyzeEntryType(qualNameEntry);
				}

				if (! (isTypeDesc instanceof ClassOrContractTypeDescriptor)) {
					throwLinePosExc(qualName, "type is not a class or contract");
				}

				composedOfTypes.add((ClassOrContractTypeDescriptor)isTypeDesc);
			}

			return new CompositeClassOrContractTypeDescriptor(nameScope, composedOfTypes);

		} else if (def instanceof Conduit_specContext) {
			/*
			conduit_spec			: Tconduit Tident? elasticity? cleanup_handler? ;
			*/

			return new ConduitTypeDescriptor();

		} else if (def instanceof Object_create_specContext) {
			/*
			object_create_spec		: Tcreate Tident Tcolon qual_name
										Tl_curl_br conduit_map_seq? Tr_curl_br
										Tl_paren expr_seq? Tr_paren ;
			*/

			/* Identify the def specified by the qual_name; it should be a class or contract: */

			Qual_nameContext qualName = ((Object_create_specContext)def).qual_name();
			List<TerminalNode> qualNameIds = qualName.Tident();
			DeclaredEntry<FlowValueType, ParseTree, TerminalNode> qualNameEntry = this.state.getDef(qualNameIds.get(qualNameIds.size()-1));
			if (qualNameEntry == null) { // attempt to resolve it now
				SymbolEntry e = this.nameResolver.resolveAndBindSymbol(
					qualNameIds, this.visibilityChecker);
				if (e == null) {
					throwLinePosExc(qualName, "Unable to identify");
				}
				qualNameEntry = (DeclaredEntry<FlowValueType, ParseTree, TerminalNode>)e;
			}

			ParseTree qualNameDef = qualNameEntry.getDefiningNode();
			if (! (qualNameDef instanceof Class_defContext) || (qualNameDef instanceof Contract_defContext)) {
				// qualName's def is not a class or contract
				throwLinePosExc(qualName, "Expected a class or contract name");
			}

			/* Determine type of the declared entry represented by qualName: */

			TypeDescriptor<FlowValueType> isTypeDesc = this.state.getTypeDesc(qualNameDef);
			if (isTypeDesc == null) { // try to determine type now:
				isTypeDesc = analyzeEntryType(qualNameEntry);
			}

			if (! (isTypeDesc instanceof ClassOrContractTypeDescriptor)) {
				throwLinePosExc(qualName, "type is not a class or contract");
			}

			return new ObjectTypeDescriptor((ClassOrContractTypeDescriptor)isTypeDesc);

		} else {
			throwLinePosExc(def,
				"Expected a value_def, type_def, class_def, contract_def, conduit_spec, or object_create_spec.");
		}

		throw new RuntimeException("Internal error: Should not get here");
	}

	/**
	 Analyze the specified type spec, which is presumed to not have a symbol entry.
	 */
	FlowTypeDescriptor analyzeTypeSpec(Value_type_specContext typeSpec) throws Exception {
		return analyzeTypeSpec(null, typeSpec);
	}


	/**
	 Analyze a method return type that specifies a conduit.
	 */
	StaticConduitTypeDescriptor analyzeCondRetTpSpec(Conduit_ret_type_specContext condRetTpSpec) throws Exception {
		/* conduit_ret_type_spec :
				Tconduit Tr_dbl_ar qual_name_seq
				//                 classref
				| qual_name_seq Tl_dbl_ar Tconduit ( Tr_dbl_ar qual_name_seq )?
				// classref                                classref
				;
			*/

		List<Qual_name_seqContext> qualNameSeqs = condRetTpSpec.qual_name_seq();

		Qual_name_seqContext inboundQualNames = null;
		Qual_name_seqContext outboundQualNames = null;


		if (condRetTpSpec.getChild(0) instanceof TerminalNode) {
			TerminalNode tn = (TerminalNode)(condRetTpSpec.getChild(0));
			outboundQualNames = qualNameSeqs.get(0);
		} else {
			inboundQualNames = qualNameSeqs.get(0);
			if (qualNameSeqs.size() > 1) {
				outboundQualNames = qualNameSeqs.get(1);
			}
		}

		/* Identify the type desc of each inbound target type qual name. */
		Set<ClassOrContractTypeDescriptor> inboundTargetTypes = null;
		if (inboundQualNames != null) {
			inboundTargetTypes = new TreeSet<ClassOrContractTypeDescriptor>();
			for (Qual_nameContext qn : inboundQualNames.qual_name()) {

				TypeDescriptor<FlowValueType> td = this.state.getTypeDesc(qn);
				if (td == null) {
					SymbolEntry entry = this.nameResolver.resolveAndBindSymbol(qn.Tident(),
						this.nameResolver.getCurrentNameScope(), this.visibilityChecker);
					if (! (entry instanceof DeclaredEntry)) throw new RuntimeException("Unexpected: SymbolEntry is not a DeclaredEntry");
					td = analyzeEntryType((DeclaredEntry<FlowValueType, ParseTree, TerminalNode>)entry);
				}
				if (! (td instanceof ClassOrContractTypeDescriptor)) {
					throwLinePosExc(qn, "FlowValueType " + td.getType() + " is not a class or contract");
				}
				inboundTargetTypes.add((ClassOrContractTypeDescriptor)td);
			}
		}

		/* Identify the type desc of each outbound target type qual name. */
		Set<ClassOrContractTypeDescriptor> outboundTargetTypes = null;
		if (outboundQualNames != null) {
			outboundTargetTypes = new TreeSet<ClassOrContractTypeDescriptor>();
			for (Qual_nameContext qn : outboundQualNames.qual_name()) {

				TypeDescriptor<FlowValueType> td = this.state.getTypeDesc(qn);
				if (td == null) {
					SymbolEntry entry = this.nameResolver.resolveAndBindSymbol(
						qn.Tident(), this.nameResolver.getCurrentNameScope(), this.visibilityChecker);
					if (! (entry instanceof DeclaredEntry)) throw new RuntimeException("Unexpected: SymbolEntry is not a DeclaredEntry");
					td = analyzeEntryType((DeclaredEntry<FlowValueType, ParseTree, TerminalNode>)entry);
				}
				if (! (td instanceof ClassOrContractTypeDescriptor)) {
					throwLinePosExc(qn, "FlowValueType " + td.getType() + " is not a class or contract");
				}
				outboundTargetTypes.add((ClassOrContractTypeDescriptor)td);
			}
		}

		return new StaticConduitTypeDescriptor(inboundTargetTypes, outboundTargetTypes);
	}


	/**
	 Interpret an intrinsic type spec and return the corresponding FlowDataType.
	 */
	FlowDataType analyzeIntrinsicType(Intrinsic_type_specContext intrinsicTypeSpec)
		throws Exception {

		FlowDataType valueType = null;

		if (intrinsicTypeSpec.Tint() != null) {
			if (intrinsicTypeSpec.sign_modifier() != null) {
				if (intrinsicTypeSpec.size_modifier() != null) {
					valueType = FlowUnsignedLongInt;
				} else {
					valueType = FlowUnsignedInt;
				}
			} else {
				if (intrinsicTypeSpec.size_modifier() != null) {
					valueType = FlowLongInt;
				} else {
					valueType = FlowInt;
				}
			}

		} else if (intrinsicTypeSpec.Tfloat() != null) {
			if (intrinsicTypeSpec.size_modifier() != null) {
				valueType = FlowLongFloat;
			} else {
				valueType = FlowFloat;
			}

		} else if (intrinsicTypeSpec.Tbyte() != null) {
			valueType = FlowByte;
		} else if (intrinsicTypeSpec.Tboolean() != null) {
			valueType = FlowBoolean;
		} else if (intrinsicTypeSpec.Tglyph() != null) {
			valueType = FlowGlyph;
		} else if (intrinsicTypeSpec.Tstring() != null) {
			valueType = FlowString;
		} else
			throwLinePosExc(intrinsicTypeSpec,
				"Unrecognized production; " + this.symbolWrapper.getText(intrinsicTypeSpec));

		return valueType;
	}

	/**
	 Given a node that defines a value or value type, find the nested value_type_spec
	 node that specifies the type.
	 */
	Value_type_specContext getValueTypeSpec(ParseTree defCtx) {
		if (defCtx instanceof Function_specContext) {
			return ((Function_specContext)defCtx).value_type_spec();
		} else if (defCtx instanceof Value_defContext) {
			return ((Value_defContext)defCtx).value_decl().value_type_spec();
		} else if (defCtx instanceof Type_defContext) {
			return ((Type_defContext)defCtx).type_decl().value_type_spec();
		} else if (defCtx instanceof Method_signatureContext) {
			Method_return_typeContext methodReturnType =
				((Method_signatureContext)defCtx).method_return_type();
			if (methodReturnType != null)
				return methodReturnType.value_type_spec();
			else
				return null;
		} else throw new RuntimeException("Unexpected node kind: " + defCtx.getClass().getName());
	}

	/**
	 The value_type_spec reduces to a qual_name - i.e., it borrows an existing type.
	 */
	private FlowTypeDescriptor analyzeType(DeclaredEntry<FlowValueType, ParseTree, TerminalNode> entry,
		Qual_nameContext qualName, Value_type_specContext valueTypeSpec) throws Exception {

		// Detect recursion: ensure it does not extend itself or an enclosing type:
		List<TerminalNode> ids1 = qualName.Tident();
		TerminalNode id = ids1.get(ids1.size()-1);
		DeclaredEntry<FlowValueType, ParseTree, TerminalNode> qualNameEntry = state.getRef(id);
		if ((entry != null) && (entryEnclosesEntry(qualNameEntry, entry))) {
			throwLinePosExc(qualName, "Recursive type definition");
		}

		// Get entry for the qualified name:
		List<TerminalNode> ids2 = qualName.Tident();
		TerminalNode borrowedTypeId = ids1.get(ids2.size()-1);
		DeclaredEntry<FlowValueType, ParseTree, TerminalNode> borrowedTypeEntry = state.getRef(borrowedTypeId);


		/* The actual type is whatever the borrowed type is. */

		TypeDescriptor<FlowValueType> borrowedTypeDesc = borrowedTypeEntry.getTypeDesc();
		if (borrowedTypeDesc == null) {
			// It has not been analyzed yet - call analyzeTypeSpec recursively:
			analyzeEntryType(borrowedTypeEntry);
			borrowedTypeDesc = borrowedTypeEntry.getTypeDesc();
		}

		if (entry != null) entry.setTypeDesc(borrowedTypeDesc);
		this.state.setTypeDesc(valueTypeSpec, borrowedTypeDesc);
		return (FlowTypeDescriptor)borrowedTypeDesc;
	}

	private FlowTypeDescriptor analyzeType(DeclaredEntry<FlowValueType, ParseTree, TerminalNode> entry,
		Struct_type_specContext structTypeSpec, Value_type_specContext valueTypeSpec) throws Exception {

		if (entry == null) {
			throwLinePosExc(structTypeSpec, "Cannot resolve struct");
		}

		StructTypeDescriptor typeDesc;
		NameScope<FlowValueType, ParseTree, TerminalNode> scope =
			((NameScopeEntry<FlowValueType, ParseTree, TerminalNode>)entry).getOwnedScope();
		SymbolTable<FlowValueType, ParseTree, TerminalNode> symtbl = scope.getSymbolTable();
		typeDesc = new StructTypeDescriptor(symtbl);
		entry.setTypeDesc(typeDesc);
		this.state.setTypeDesc(valueTypeSpec, typeDesc);
		entry.addImplementedNameScope(scope);

		/* Analyze type of each field of the struct: */
		Struct_type_spec_scopeContext structTypeSpecScope = structTypeSpec.struct_type_spec_scope();
		Struct_elt_seqContext structEltSeq = structTypeSpecScope.struct_init().struct_elt_seq();
		if (structEltSeq != null) {
			for (Struct_eltContext structElt : structEltSeq.struct_elt()) {
				DeclaredEntry<FlowValueType, ParseTree, TerminalNode> elementEntry = this.state.getDef(structElt);

				// Detect recursion:
				if (entryEnclosesEntry(elementEntry, entry)) {
					throwLinePosExc(elementEntry.getDefiningNode(), "Recursive type definition");
				}

				Value_type_specContext eltTypeSpec = null;
				if (structElt.value_def() != null) {
					eltTypeSpec = getValueTypeSpec(structElt.value_def());
				} else if (structElt.function_def() != null) {
					eltTypeSpec = getValueTypeSpec(structElt.function_def().function_spec());
				} else throwLinePosExc(structElt, "Unexpected production");

				if (eltTypeSpec == null) {
					throw new RuntimeException("Unable to resolve type spec for '" + structElt.getText() + "'");
				}
				analyzeTypeSpec(elementEntry, eltTypeSpec);
			}
		}

		if (structTypeSpec.qual_name_seq() == null) return typeDesc;

		/* modifies or adds to an existing struct type.
			struct_type_spec	: qual_name_seq? value_type_spec_scope
			*/

		// Identify each borrowed scope:
		Qual_name_seqContext qualNameSeq = structTypeSpec.qual_name_seq();
		Set<DataTypeDescriptor> borrowedTypes = new TreeSet<DataTypeDescriptor>();
		for (Qual_nameContext qualName : qualNameSeq.qual_name()) {

			List<TerminalNode> ids = qualName.Tident();
			TerminalNode id = ids.get(ids.size()-1);
			DeclaredEntry<FlowValueType, ParseTree, TerminalNode> borrowedTypeEntry = state.getRef(id);

			// Verify that the qual name's type has been analyzed (otherwise,
			// we won't know its type).
			ValueType borrowedType;
			try { borrowedType = borrowedTypeEntry.getType(); }
			catch (UnknownTypeException ex) { // has not been analyzed yet
				analyzeEntryType(borrowedTypeEntry);
				borrowedType = borrowedTypeEntry.getType();
			}

			// Verify that the qualified name represents a struct type:
			if (borrowedType != FlowStruct) {
				throwLinePosExc(id, "Excpected a struct type");
			}
			assertIsA(borrowedTypeEntry, NameScopeEntry.class);

			// Add the scope (which the qualified name references) to the set of borrowed scopes:
			NameScope<FlowValueType, ParseTree, TerminalNode> borrowedScope = ((NameScopeEntry)borrowedTypeEntry).getOwnedScope();
			if (! (borrowedType instanceof DataTypeDescriptor)) throwLinePosExc(
				qualName, "A struct extension type must be a data value type");
			borrowedTypes.add((DataTypeDescriptor)borrowedType);

			// Add the borrowed scope to the entry's new scope:
			entry.addImplementedNameScope(borrowedScope);
		}

		// Detect recursion among the struct fields:
		Struct_type_spec_scopeContext structTypeSpecScope3 = structTypeSpec.struct_type_spec_scope();
		Struct_elt_seqContext structEltSeq2 = structTypeSpecScope3.struct_init().struct_elt_seq();
		if (structEltSeq2 != null) {
			for (Struct_eltContext structElt : structEltSeq2.struct_elt()) { // for each struct field
				DeclaredEntry<FlowValueType, ParseTree, TerminalNode> elementEntry = this.state.getDef(structElt);
				if (entryEnclosesEntry(elementEntry, entry)) {
					throwLinePosExc(elementEntry.getDefiningNode(), "Recursive type definition");
				}
			}
		}

		// Construct a composite type desc, consisting of the value_type_spec_scope's scope
		// and the borrowed scopes:
		FlowTypeDescriptor compositeTypeDesc = new StructExtensionTypeDescriptor(
			typeDesc.symbolTable,	// SymbolTable<FlowValueType, Node, TId> symbolTable
			borrowedTypes 	// Set<DataTypeDescriptor> borrowedTypes
			);

		// Set the type desc of the entry that we are analying:
		entry.setTypeDesc(compositeTypeDesc);

		this.state.setTypeDesc(valueTypeSpec, compositeTypeDesc);

		return compositeTypeDesc;
	}

	private FlowTypeDescriptor analyzeType(DeclaredEntry<FlowValueType, ParseTree, TerminalNode> entry,
		Enum_spec_scopeContext enumSpecScope, Value_type_specContext valueTypeSpec) throws Exception {

		// Detect recursion:
		for (Enum_value_defContext enumValueDef : enumSpecScope.enum_value_def()) {
			TerminalNode id = enumValueDef.Tident();
			DeclaredEntry<FlowValueType, ParseTree, TerminalNode> enumValueEntry = state.getRef(id);
			if ((entry != null) && entryEnclosesEntry(enumValueEntry, entry)) {
				throwLinePosExc(enumValueDef, "Recursive type definition");
			}

			// Provide a warning if an enum value hides an existing name - this
			// warning is important because enum values can be referenced without
			// qualifying them with the name of the enum type:
			if (entry != null) {
				NameScope<FlowValueType, ParseTree, TerminalNode> entryNamescope = entry.getEnclosingScope();
				List<SymbolEntry<FlowValueType, ParseTree, TerminalNode>> es =
					nameResolver.identifySymbols(id, entryNamescope, null);
				for (SymbolEntry<FlowValueType, ParseTree, TerminalNode> e : es) {
					if (e != entry) {
						this.state.addLinePosWarn(id,
							"Token " + this.symbolWrapper.getText(id) +
							" hides an identifier" +
							(e instanceof DeclaredEntry ? " declared at line " +
								this.symbolWrapper.getLine(
								((DeclaredEntry<FlowValueType, ParseTree, TerminalNode>)e).getDefiningNode())
								: ""
							)
						);
						break;
					}
				}
			}
		}

		NameScope<FlowValueType, ParseTree, TerminalNode> enumScope = this.state.getScope(enumSpecScope);
		FlowTypeDescriptor typeDesc = new EnumTypeDescriptor(enumScope.getSymbolTable());

		if (entry != null) {
			assertIsA(entry, NameScopeEntry.class);
			entry.setTypeDesc(typeDesc);
			this.state.setTypeDesc(valueTypeSpec, typeDesc);
			NameScope<FlowValueType, ParseTree, TerminalNode> scope =
				((NameScopeEntry<FlowValueType, ParseTree, TerminalNode>)entry).getOwnedScope();
			entry.addImplementedNameScope(scope);
		}

		return typeDesc;
	}

	private FlowTypeDescriptor analyzeType(DeclaredEntry<FlowValueType, ParseTree, TerminalNode> entry,
		List<Array_def_specContext> arrayDefSpecs, Value_type_specContext valueTypeSpec) throws Exception {

		/*	value_type_spec	: array_elt_type_spec array_def_spec+ ; */
		Array_elt_type_specContext eltTypeSpec = valueTypeSpec.array_elt_type_spec();

		/* Determine the element type:
			array_elt_type_spec : intrinsic_type_spec | qual_name ;
			*/
		DataTypeDescriptor eltTypeDesc = null;
		if (eltTypeSpec.intrinsic_type_spec() != null) {
			FlowDataType eltValueType = analyzeIntrinsicType(eltTypeSpec.intrinsic_type_spec());
			eltTypeDesc = new IntrinsicTypeDescriptor(eltValueType);
		} else if (eltTypeSpec.qual_name() != null) {
			// Get entry for the qualified name:
			List<TerminalNode> ids = eltTypeSpec.qual_name().Tident();
			TerminalNode eltTypeId = ids.get(ids.size()-1);
			DeclaredEntry<FlowValueType, ParseTree, TerminalNode> eltTypeEntry = state.getRef(eltTypeId);

			TypeDescriptor<FlowValueType> et = eltTypeEntry.getTypeDesc(); // TypeDescriptor<FlowValueType>
			if (et == null) {
				// It has not been analyzed yet - call analyzeTypeSpec recursively:
				analyzeEntryType(eltTypeEntry);
				et = eltTypeEntry.getTypeDesc();
				if (et == null) throwLinePosExc(eltTypeSpec.qual_name(), "Unable to resolve type");

			} else {
				if (! (et instanceof DataTypeDescriptor)) {
					throwLinePosExc(eltTypeSpec.qual_name(), "Must be a data value type");
				}
				eltTypeDesc = (DataTypeDescriptor)et;

				// Just re-use the type desc that is being borrowed:
				eltTypeEntry.setTypeDesc(eltTypeDesc);
				this.state.setTypeDesc(eltTypeSpec, eltTypeDesc);
			}

			// Detect recursion: determine of the array element type is an enclosing type:
			if ((entry != null) && entryEnclosesEntry(eltTypeEntry, entry)) {
				throwLinePosExc(eltTypeSpec.qual_name(), "Recursive type definition");
			}

		} else throwLinePosExc(eltTypeSpec, "Unexpected production");

		if (eltTypeDesc == null) throwLinePosExc(eltTypeSpec, "Indeterminate element type");


		/* Locate the initialization expression: */

		ParserRuleContext definingNode = getDefiningNode(valueTypeSpec);
			/* ->
				method_return_type	.getParent()		-> method_signature (error)
				function_spec		.getParent()		-> function_def (error)
				value_decl			.getParent()		-> arg_def (error) | value_def
				type_decl			.getParent()		-> type_def (error)
				*/
		if (! (definingNode instanceof Value_defContext)) {
			throwLinePosExc(valueTypeSpec, "Unable to determine array range");
		}

		ParserRuleContext parent = definingNode.getParent();
			/* ->
				namespace_elt	: value_def initialization? | ...
					initialization	: Tl_ar expr Tsemicolon ;
				class_elt		: value_def initialization? | ...

				struct_elt		: value_def ( Tl_ar expr )? | function_def ;

				expr			: expr array_spec  // for selecting array elements
								| expr Tperiod Tident // a struct field selection
								| value_def left_assign_op expr
								| expr right_assign_op value_def
								| ...

				//expr			: value_def left_assign_op expr...
				//					| expr right_assign_op value_def...
				*/

		ExprContext initExpr = null; // may not be null
		if (parent instanceof Namespace_eltContext) {
			InitializationContext init = ((Namespace_eltContext)parent).initialization();
			if (init != null) initExpr = init.expr();
		} else if (parent instanceof Struct_eltContext) {
			initExpr = ((Struct_eltContext)parent).expr();
		} else if (parent instanceof Class_eltContext) {
			InitializationContext init = ((Class_eltContext)parent).initialization();
			if (init != null) initExpr = init.expr();
		} else {
			throwLinePosExc(parent,
				"Expected a namespace element, struct element, or class element");
		}

		if (initExpr == null) {
			throwLinePosExc(definingNode, "No initialization expression found");
		}

		/* Any dimension range that is not specified by the type spec is considered to be dynamic.
			Ref. LRM, "Multi-Dimensional Arrays".
			*/
		DataTypeDescriptor typeDesc = eltTypeDesc;  // might be null
		for (Array_def_specContext arrayDefSpec : arrayDefSpecs) { // each array range of the type spec

			ExprContext indexExpr = arrayDefSpec.expr();

			Long size;

			if (indexExpr == null) {  // dimension is dynamic
				size = null;

			} else {  // size of the dimension is specified

				// Expr must be statically determinable
				ExprAnnotation indexExprAnnot = this.state.getVal(indexExpr);
				if (indexExprAnnot == null) {
					throwLinePosExc(indexExpr,
						"Declared array dimensions, if not null (dynamic), must be statically evaluatable");
				}

				Object obj = indexExprAnnot.getValue();
				if (! (obj instanceof FlowValue)) throwLinePosExc(indexExpr, "Expected a value");
				FlowValue declaredDim = (FlowValue)obj;

				if (! declaredDim.valueIsIntegral()) {
					throwLinePosExc(indexExpr, "Must be an integral value");
				}
				if (! declaredDim.valueIsJavaIntCompatible()) {
					throwLinePosExc(indexExpr,
						"Array size must be integral and may not be larger than " + Integer.MAX_VALUE);
				}
				if (! declaredDim.isPositiveNumber()) {
					throwLinePosExc(indexExpr, "Array size must be a positive number");
				}

				FlowUnsignedLongIntValue convertedValue = null;
				try { convertedValue = (FlowUnsignedLongIntValue)(declaredDim.convertTo(FlowUnsignedLongInt)); }
				catch (ConversionException ex) {
					throwLinePosExc(indexExpr, ex.getMessage());
				}

				size = convertedValue.value;
			}

			typeDesc = new ArrayTypeDescriptor(typeDesc, size);
		}

		if (entry != null) entry.setTypeDesc(typeDesc);
		this.state.setTypeDesc(valueTypeSpec, typeDesc);
		return typeDesc;
	}

	/**
	 Add the specified node to the set of nodes that have been type analyzed.
	 This enables us to avoid type analyzing a node more than once, even if the
	 node was not able to yield a type.
	 */
	private void setAnalyzed(ParseTree node) {
		analyzedSet.add(node);
	}

	/**
	 Return true if the specified node was visited for type analyzis.
	 */
	private boolean hasBeenAnalyzed(ParseTree node) {
		return analyzedSet.contains(node);
	}

	/**
	 Convenience method: calls hasBeenAnalyzed and then setAnalyzed.
	 */
	private boolean checkAnalyzed(ParseTree node) {
		if (hasBeenAnalyzed(node)) return true;
		setAnalyzed(node);
		return false;
	}

	private FlowTypeDescriptor analyzeType(DeclaredEntry<FlowValueType, ParseTree, TerminalNode> entry,
		Intrinsic_type_specContext intrinsicTypeSpec) throws Exception {

		FlowDataType valueType = analyzeIntrinsicType(intrinsicTypeSpec);
		FlowTypeDescriptor typeDesc = new IntrinsicTypeDescriptor(valueType);
		if (entry != null) entry.setTypeDesc(typeDesc);
		this.state.setTypeDesc(intrinsicTypeSpec, typeDesc);
		return typeDesc;
	}

	/**
	 Given a value_type_spec, find the enclosing node that defines the entity whose
	 type is defined by the value_type_spec.
	 */
	private ParserRuleContext getDefiningNode(Value_type_specContext valueTypeSpec) {

		ParserRuleContext parent = valueTypeSpec.getParent();
		if (parent instanceof Method_return_typeContext) {
			//method_return_type .getParent() -> method_signature
			ParserRuleContext parent2 = parent.getParent();
			assertIsA(parent2, Method_signatureContext.class);
			return parent2;
		} else if (parent instanceof Function_specContext) {
			//function_spec .getParent() -> function_def
			ParserRuleContext parent2 = parent.getParent();
			assertIsA(parent2, Function_defContext.class);
			return parent2;
		} else if (parent instanceof Value_declContext) {
			//value_decl .getParent() ->
			//	arg_def
			//	value_def
			ParserRuleContext parent2 = parent.getParent();
			if (parent2 instanceof Arg_defContext) return parent2;
			else if (parent2 instanceof Value_defContext) return parent2;
			else
				throw new RuntimeException("Unexpected node type: " + parent2.getClass().getName());
		} else if (parent instanceof Type_declContext) {
			//type_decl .getParent() -> type_def
			ParserRuleContext parent2 = parent.getParent();
			return parent2;
		} else if (parent instanceof Value_type_specContext) {
			//value_type_spec - only happens for arrays
			assertThat(((Value_type_specContext)parent).array_def_spec() != null,
				"Expected parent to have an array spec but it doesn't");
			return getDefiningNode(valueTypeSpec); // call recursively
		} else
			throw new RuntimeException("Unexpected node type: " + parent.getClass().getName());
	}

	/**
	 Return true if 'possiblyEnclosingEntry' defines a scope that encloses 'entry'.
	 */
	private boolean entryEnclosesEntry(DeclaredEntry<FlowValueType, ParseTree, TerminalNode> possiblyEnclosingEntry,
		DeclaredEntry<FlowValueType, ParseTree, TerminalNode> entry) {

		if (! (possiblyEnclosingEntry instanceof NameScopeEntry)) return false;

		DeclaredEntry<FlowValueType, ParseTree, TerminalNode> curEntry = entry;
		for (;;) {
			NameScope enclScope = curEntry.getEnclosingScope();
			if (enclScope == null) return false;

			NameScopeEntry enclEntry = enclScope.getSelfEntry();
			if (enclEntry == possiblyEnclosingEntry) return true;
			curEntry = enclEntry;
		}
	}

	private void throwLinePosExc(ParseTree node, String msg) {
		this.state.throwLinePosExc(node, msg);
	}

	private void throwLinePosExc(ParseTree node, Exception ex) {
		this.state.throwLinePosExc(node, ex);
	}
}
