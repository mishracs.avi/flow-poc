package cliffberg.flow.analyzer;

import cliffberg.symboltable.SymbolEntry;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;

public class VTableEntry {
	public SymbolEntry<FlowValueType, ParseTree, TerminalNode> symbolEntry;
	public boolean isVariadic;
	public SymbolEntry<FlowValueType, ParseTree, TerminalNode>[] argEntries; // one for each formal arg
	public FlowTypeDescriptor[] argTypes; // one for each formal arg
	public FlowTypeDescriptor returnType; // not used for method binding

	public VTableEntry(SymbolEntry<FlowValueType, ParseTree, TerminalNode> symbolEntry, boolean isVariadic,
		SymbolEntry<FlowValueType, ParseTree, TerminalNode>[] argEntries, FlowTypeDescriptor[] argTypes,
		FlowTypeDescriptor returnType) {

		this.symbolEntry = symbolEntry;
		this.isVariadic = isVariadic;
		this.argEntries = argEntries;
		this.argTypes = argTypes;
		this.returnType = returnType;
	}
}
