# Written by Cliff Berg.

# This makefile contains no information about file structure or tool locations.
# All such configurations should be made in makefile.inc

include makefile.inc

.ONESHELL:
.SUFFIXES:

# Artifact names:
export PRODUCT_NAME := Flow POC Compiler
export Description := Flow Compiler - Proof of Concept

# Command aliases:
export SHELL := /bin/sh
export JAVA := $(JAVA_HOME)/bin/java

# Relative locations:
export ThisDir := $(shell pwd)
export parser_classes := $(MVN_REPO)/com/cliffberg/flow/parser/0.1/parser-0.1.jar
export analyzer_classes := $(MVN_REPO)/com/cliffberg/flow/analyzer/0.1/analyzer-0.1.jar
export generator_classes := $(MVN_REPO)/com/cliffberg/flow/generator/0.1/generator-0.1.jar
export main_classes := $(MVN_REPO)/com/cliffberg/flow/main/0.1/main-0.1.jar
export standard_classes := $(MVN_REPO)/com/cliffberg/flow/standard/0.1/standard-0.1.jar
export symboltable_classes := $(MVN_REPO)/com/cliffberg/symboltable/symboltable/0.1/symboltable-0.1.jar
export util_classes := $(MVN_REPO)/com/cliffberg/utilities/utilities/0.1/utilities-0.1.jar
export test_classes := $(MVN_REPO)/com/cliffberg/flow/test/0.1/test-0.1.jar
export javadoc_dir := javadocs
export AntlrJar := $(AntlrDir)/antlr-4.7.1-complete.jar
export ByteBuddyJar := $(MVN_REPO)/net/bytebuddy/byte-buddy/1.9.2/byte-buddy-1.9.2.jar

flow_classes := $(parser_classes):$(analyzer_classes):$(generator_classes):$(main_classes):$(standard_classes):$(symboltable_classes):$(util_classes)

# Aliases:
test := $(JAVA) -cp $(CUCUMBER_CLASSPATH):$(test_classes):$(flow_classes):$(AntlrJar):$(ByteBuddyJar)
test := $(test) cucumber.api.cli.Main --glue cliffberg.flow.test
test := $(test) test/features


################################################################################
# Tasks


.PHONY: all gen_config grammar parser analyzer standard generator main test test_all javadoc mvnversion cukehelp cukever clean info

all: gen_config build test_all javadoc

mvnversion:
	$(MVN) --version


# ------------------------------------------------------------------------------
# Build everything.
build:
	$(MVN) clean install


# ------------------------------------------------------------------------------
# Generate the Config class that the runtime uses to determine the version of DABL.
gen_config:
	echo "package cliffberg.flow;" > java/cliffberg/flow/Config.java
	echo "public class Config {" >> java/cliffberg/flow/Config.java
	echo "public static final String FlowCompilerVersion = \"$(FLOW_COMPILER_VERSION)\";" >> java/cliffberg/flow/Config.java
	echo "public static final String FlowLanguageVersion = \"$(FLOW_LANGUAGE_VERSION)\";" >> java/cliffberg/flow/Config.java
	echo "}" >> java/cliffberg/flow/Config.java


# ------------------------------------------------------------------------------
# Process grammar to create parser.
grammar:
	rm -rf parser/java/*
	echo JAVA=$(JAVA)
	echo ThisDir=$(ThisDir)
	$(JAVA) -jar $(AntlrJar) \
		-o $(ThisDir)/parser/java/cliffberg/flow/parser \
		-lib $(ThisDir)/parser/java \
		-package cliffberg.flow.parser \
		Flow.g4


# ------------------------------------------------------------------------------
# Compile the generated parser.
parser: grammar
	$(MVN) clean install --projects parser


# ------------------------------------------------------------------------------
# Compile the Analyzer.
analyzer:
	$(MVN) clean install --projects analyzer


# ------------------------------------------------------------------------------
# Compile the standard Flow packages.
standard:
	$(MVN) clean install --projects standard


# ------------------------------------------------------------------------------
# Compile the Generator.
generator:
	$(MVN) clean install --projects generator


# ------------------------------------------------------------------------------
# Compile the main program.
main:
	$(MVN) clean install --projects main


# ------------------------------------------------------------------------------
# For development: Compile only the behavioral tests.
test:
	$(MVN) clean install --projects test


# ------------------------------------------------------------------------------
# Generate javadocs for all modules.
javadoc:
	$(MVN) javadoc:javadoc


# ------------------------------------------------------------------------------
# Run Cucumber tests.
#	Gherkin tags: done, smoke, notdone, docker, exec, unit, pushlocalrepo, task,
#	patternsets, inputsandoutputs

test_all:
	$(test)

# To split output, do:
#	make test_tagged  > >(tee -a stdout.log) 2> >(tee -a stderr.log >&2)
test_tagged:
	echo test=$(test)
	$(test) --tags @calculator

cukehelp:
	$(JAVA) -cp $(CUCUMBER_CLASSPATH) cucumber.api.cli.Main --help

cukever:
	$(JAVA) -cp $(CUCUMBER_CLASSPATH) cucumber.api.cli.Main --version

calc:
	$(JAVA) -cp $(flow_classes):$(AntlrJar):$(ByteBuddyJar):$(MVN_REPO)/net/sf/jopt-simple/jopt-simple/5.0.4/jopt-simple-5.0.4.jar \
		cliffberg.flow.main.Main --tree --parseOnly --noGen --omitStandard test/other/command/calculator.flow



# ------------------------------------------------------------------------------
clean: compile_clean test_clean clean_parser
	$(MVN) clean

info:
	@echo "Makefile for $(PRODUCT_NAME)"
