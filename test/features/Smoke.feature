# language: en

Feature: Smoke
	
	@done
	Scenario: Simple
		Given trivial input
		"""
		namespace MyNamespace:
		"""
		
		Then the smoke input is parsed without error
		
		And the smoke test analyzes without an error
		
		And a Global namespace entry called MyNamespace is made
		
		And the smoke test generates a JVM class called MyNamespace
