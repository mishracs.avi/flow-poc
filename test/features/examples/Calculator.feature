# language: en

@calculator
Feature: Calculator
	
	@done
	Scenario: Calculator
		Given a calculator program
		"""
namespace cliffberg.calculator:

type Command: {
	op: enum { plus, minus, clear },
	data: int
}

function parseCommand(line: string) Command ^Exception {
	return (case line[0] {
		'c': Command{op <- clear};
			/* "Command" should be identified in the namespace symbol table.
			 Upon identification, it should be pushed as the current namespace
			 for the duration of the scope of the Command construct.
			 "op" should be identified as the field in the Command type.*/
		
		
		'+': Command{op <- plus, data <- int(line[1..])};
		'-': Command{op <- minus, data <- int(line[1..])};
	});
}

class MyClass {
	method main(args: string[]) ^Exception {
		conduit con --> [0] create accumulator: Accumulator{}();
		for ever {
			stdin -> line: string ;
			value command: Command <- parseCommand(line);
			value total: int <- ( case command.op {
				plus: con.add(command.data);
				minus: con.subtract(command.data);
				clear: con.clear();
			});
			total -> stdout;
		}
	}
}

class Accumulator {
	value total: int <- 0;
	method add(n: int) int {
		return total <- total + n;
	}
	method subtract(n: int) int {
		return total <- total - n;
	}
	method clear() {
		total <- 0;
	}
}
		"""
		
		#Then the calculator test is parsed without error
		
		And the calculator test analyzes without an error
		
		#And a Global namespace entry called cliffberg.calculator is made
		
		#And that namespace contains a class called MyClass
		
		#And it also contains a class called Accumulator
		
		#And it also contains a function called parseCommand
		
		And it also contains a type called Command
		
		And type Command contains two fields, called op and data
		
		And function parseCommand contains a return statement
		
		And class MyClass contains a method called main
		
		And main contains a conduit called con
		
		And main contains a for statement
		
		And the for statement defines a value called line
		
		And it also defines a value called command
		
		And the type of command is Command
		
		
		#And the calculator test generates ....
