Feature: Expression Parser
	
	@done
	Scenario: Expression Parser
		Given an expression parser program
		"""
namespace cliffberg.flow.examples.expr_parser:

method main(args: String[]) int {
	try root --> parseExpression();
	catch (NoMoreTokens) {
		//....
	}
	catch (SyntaxException) {
		"Input error" -> stdout;
		return 1;
	}
	root.walk();  // “executes” the parse tree
}

conduit root --> Node;
value token: string;
conduit walker --> create Accumulator{walker}();

internal method nextToken() ^ NoMoreTokens {
	value foundToken <- false;
	for ever {
		try c: glyph <- stdin;
				catch (ex: IOException) {
					if foundToken return;
					else throw NoMoreTokens;
				}
	if isWhitespace(c)
	if foundToken return;
	else continue;
		foundToken <- true;
		this.token +< c;
	}
}

private method putTokenBack() { //....
}

private method parseExpression() conduit --> Expression {
	conduit num --> parseNumber();
	nextToken();
	if token = "+" or token = "-" {
		// Production is: expression ::= number op expression
		conduit expr2 --> parseExpression();
		create binExpr: BinaryExpression{walker, num,
		expr2}(token);
		return conduit con --> binExpr;
	} else {
		// Production is: expression ::= number
		// Reduce the Expression production, to produce
		// an Expression, and return a conduit to that.
		putTokenBack();
		return num;
	}
}

private method parseNumber() conduit --> Number ^ TokenNotANumber {
	nextToken();
	create number: Number{walker}(token);
	return conduit con --> number;
}

function isWhitespace(c: glyph) boolean {
	if c = ' ' return true;
	if c = '\t' return true;
	return false;
}

class Node {
	abstract method walk();
}

class Expression extends Node {
}

class Number: Expression {
	Number{walker --> Walker}(token: string) {
		this.value <- int(token);
	}
	method walk() {
		walker.enterNumber();
		walker.leaveNumber();
	}
	method getValue() int {
		return this.value;
	}
}

class BinaryExpression: Expression {
	BinaryExpression{
		walker --> Walker,
		conduit num --> Number,
		conduit expr --> Expression} (operator: string) {
	}
	method walk() {
		walker.enterBinaryExpression();
		num.walk();
		expr.walk();
		walker.leaveBinaryExpression();
	}
}

class Walker {
	Walker{root --> Node}() {
	}
	method walk() {
		root.walk();
	}
	replaceable method enterNumber() {}
	replaceable method leaveNumber() {}
	replaceable method enterBinaryExpression() {}
	replaceable method leaveBinaryExpression() {}
}

class Accumulator: Walker {
	
	Accumulator{allNodes[] <--> Expression}() {
	}

	method enterNumber() {
	}
	method leaveNumber() throws Exception {
		push((allNodes[active] supports Number).getValue());
	}
	method enterBinaryExpression() {
	}
	method leaveBinaryExpression() {
		push
	}
	private method push(value: int) {
		//....
	}
	private method pop() int {
		//....
	}
}
		"""
		
		Then the expression test is parsed without error
		
		And the expression test analyzes without an error
		
		#And the expression test generates ....
