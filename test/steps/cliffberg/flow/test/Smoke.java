package cliffberg.flow.test;

import cliffberg.flow.test.TestBase;
import cliffberg.flow.parser.FlowParser.NamespaceContext;

import cliffberg.symboltable.NameScope;
import cliffberg.symboltable.SymbolEntry;
import cliffberg.symboltable.NameScopeEntry;

import static cliffberg.utilities.AssertionUtilities.*;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;

import net.bytebuddy.dynamic.DynamicType;

import cucumber.api.Format;
import cucumber.api.java.Before;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Smoke extends TestBase {
	
	@Given("^trivial input$")
	public void trivial_input(String source) throws Exception {
		setSource(source);
	}
	
	@Then("^the smoke input is parsed without error$")
	public void the_smoke_input_is_parsed_without_error() throws Exception {
		checkParse();
	}

	@Then("^the smoke test analyzes without an error$")
	public void the_smoke_test_analyzes_without_error() throws Exception {
		checkAnalyze();
	}
	
	@Then("^a Global namespace entry called MyNamespace is made$")
	public void a_Global_namespace_entry_called_MyNamespace_is_made() throws Exception {
		
		// Check that there is a name scope entry in the global name scope,
		// and that the entry is a name scope called 'MyNamespace'.
		SymbolEntry entry = getState().getGlobalScope().getEntry("MyNamespace");
		assertThat(entry != null, "No entry named 'MyNamespace' in global scope");
		assertIsA(entry, NameScopeEntry.class);
		NameScope ownedScope = ((NameScopeEntry)entry).getOwnedScope();
		assertThat(ownedScope != null, "Entry for MyNamespace has no owned scope");
		assertThat(ownedScope.getName().equals("MyNamespace"),
			"Name of owned scope is not 'MyNamespace: it is " + ownedScope.getName());
	}

	@Then("^the smoke test generates a JVM class called MyNamespace$")
	public void the_smoke_test_generates_a_JVM_class_called() throws Exception {
		checkGenerate();
		
		// Check that the right classes were generated.
		DynamicType dt = getGenerator().getClasses().get("MyNamespace");
		assertThat(dt != null, "Class MyNamespace not found");
	}
}
