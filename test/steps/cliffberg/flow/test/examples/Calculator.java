package cliffberg.flow.test.examples;

import cliffberg.flow.test.TestBase;

import static cliffberg.flow.parser.FlowParser.*;
import static cliffberg.flow.analyzer.FlowSymbolWrapper.*;

import cliffberg.symboltable.NameScope;
import cliffberg.symboltable.NameScopeEntry;
import cliffberg.symboltable.SymbolEntry;
import cliffberg.symboltable.DeclaredEntry;
import cliffberg.symboltable.CompilerState;

import static cliffberg.utilities.AssertionUtilities.*;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;

import cucumber.api.Format;
import cucumber.api.java.Before;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.List;

public class Calculator extends TestBase {
	
	private NameScope namespaceScope;
	
	protected NameScope getNamespaceScope() {
		return this.namespaceScope;
	}
	
	protected NameScope getNamespaceScope(String scopeName) {
		DeclaredEntry<ParseTree> entry = getState().getGlobalScope().getDeclaredEntry(scopeName);
		assertThat(entry != null, "No entry named '" + scopeName + "' in global scope");
		return ((NameScopeEntry)entry).getOwnedScope();
	}
	
	protected void setNamespaceScope(NameScope scope) {
		this.namespaceScope = scope;
	}
	
	@Given("^a calculator program$")
	public void a_calculator_program(String source) throws Throwable {
		setSource(source);
	}

	@Then("^the calculator test is parsed without error$")
	public void the_calculator_test_is_parsed_without_error() throws Throwable {
		checkParse();
	}
		
	@Then("^the calculator test analyzes without an error$")
	public void the_calculator_test_analyzes_without_an_error() throws Throwable {
		handleError(() -> {
			try { checkAnalyze(); } catch (Exception ex) { throw new RuntimeException(ex); }
			/*
			List<ParseTree> trees = getState().getASTs();
			System.err.println("ASTs (" + trees.size() + "):");
			for (ParseTree tree : trees) {
				printTree(tree, System.err, 0);
			}
			*/
			return null;
		});
	}
	
	@Then("^a Global namespace entry called cliffberg\\.calculator is made$")
	public void a_Global_namespace_entry_called_cliffberg_calculator_is_made() throws Throwable {
		
		handleError(() -> {
			// Check that there is a name scope entry 'cliffberg.calculator' in the global name scope,
			DeclaredEntry<ParseTree> entry = getState().getGlobalScope().getDeclaredEntry(
				"cliffberg.calculator");
			assertThat(entry != null, "No entry named 'cliffberg.calculator' in global scope");
			setNamespaceScope(((NameScopeEntry)entry).getOwnedScope());
			assertThat(this.getNamespaceScope() != null, "Entry for cliffberg.calculator has no owned scope");
			assertThat(this.getNamespaceScope().getName().equals("cliffberg.calculator"),
				"Name of owned scope is not 'cliffberg.calculator: it is " + this.getNamespaceScope().getName());
			return null;
		});
	}
	
	@Then("^that namespace contains a class called MyClass$")
	public void that_namespace_contains_a_class_called_MyClass() throws Throwable {
		
		handleError(() -> {
			DeclaredEntry<ParseTree> myClassEntry = this.getNamespaceScope(
				"cliffberg.calculator").getDeclaredEntry("MyClass");
			assertThat(myClassEntry != null, "Entry for MyClass not found");
			ParseTree node = myClassEntry.getDefiningNode();
			assertThat(node instanceof Class_defContext,
				"MyClass is not defined as a class: it is a " + node.getClass().getName());
			return null;
		});
	}
	
	@Then("^it also contains a class called Accumulator$")
	public void it_also_contains_a_class_called_Accumulator() throws Throwable {
		
		handleError(() -> {
			DeclaredEntry<ParseTree> accumulatorEntry = this.getNamespaceScope(
				"cliffberg.calculator").getDeclaredEntry("Accumulator");
			assertThat(accumulatorEntry != null, "Entry for Accumulator not found");
			ParseTree node = accumulatorEntry.getDefiningNode();
			assertThat(node instanceof Class_defContext,
				"Accumulator is not defined as a class: it is a " + node.getClass().getName());
			return null;
		});
	}
	
	@Then("^it also contains a function called parseCommand$")
	public void it_also_contains_a_function_called_parseCommand() throws Throwable {
		
		handleError(() -> {
			DeclaredEntry<ParseTree> parseCommandEntry = this.getNamespaceScope(
				"cliffberg.calculator").getDeclaredEntry("parseCommand");
			assertThat(parseCommandEntry != null, "Entry for parseCommand not found");
			ParseTree node = parseCommandEntry.getDefiningNode();
			assertThat(node instanceof Function_defContext,
				"parseCommand is not defined as a function: it is a " + node.getClass().getName());
			return null;
		});
	}
	
	@Then("^it also contains a type called Command$")
	public void it_also_contains_a_type_called_Command() throws Throwable {
		
		handleError(() -> {
			DeclaredEntry<ParseTree> commandEntry = this.getNamespaceScope(
				"cliffberg.calculator").getDeclaredEntry("Command");
			assertThat(commandEntry != null, "Entry for Command not found");
			ParseTree node = commandEntry.getDefiningNode();
			assertThat(node instanceof Type_defContext,
				"Command is not defined as a type: it is a " + node.getClass().getName());
			return null;
		});
	}
	
	@Then("^type Command contains two fields, called op and data$")
	public void type_Command_contains_two_fields_called_op_and_data() throws Throwable {
		
		throw new Exception("Pending");
	}
	
	@Then("^function parseCommand contains a return statement$")
	public void function_parseCommand_contains_a_return_statement() throws Throwable {
		
		throw new Exception("Pending");
	}
	
	@Then("^class MyClass contains a method called main$")
	public void class_MyClass_contains_a_method_called_main() throws Throwable {
		
		throw new Exception("Pending");
	}
	
	@Then("^main contains a conduit called con$")
	public void main_contains_a_conduit_called_con() throws Throwable {
		
		throw new Exception("Pending");
	}
	
	@Then("^main contains a for statement$")
	public void main_contains_a_for_statement() throws Throwable {
		
		throw new Exception("Pending");
	}
	
	@Then("^the for statement defines a value called line$")
	public void the_for_statement_defines_a_value_called_line() throws Throwable {
		
		throw new Exception("Pending");
	}
	
	@Then("^it also defines a value called command$")
	public void it_also_defines_a_value_called_command() throws Throwable {
		
		throw new Exception("Pending");
	}
	
	@Then("^the type of command is Command$")
	public void the_type_of_command_is_Command() throws Throwable {
		
		throw new Exception("Pending");
	}
	
	/*
	@Then("^the calculator test generates ....$")
	public void the_calculator_test_generates_....() throws Throwable {
		checkGenerate();
		....
	}
	*/
}
